class RemoveUserIdFromList < ActiveRecord::Migration[5.2]
  def change
    remove_column :lists, :user_id, :integer
  end
end
