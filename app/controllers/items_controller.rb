class ItemsController < ApplicationController
  
  # 新規作成処理
  def create
    list = List.find(params[:list_id])
    @item = list.items.build(item_params)
    if @item.save!
      redirect_to list
    else
      edirect_to list, notice: "登録を失敗しました。"
    end
  end

  def destroy
    item = Item.find(params[:id])
    item.destroy
    redirect_back(fallback_location: items_path)
  end

  private

  # リクエストパラメータ
  def item_params
    params.require(:item).permit(:name, :amount)
  end
end
