class ListsController < ApplicationController
  # 一覧画面表示
  def index
    logged_in_user
    @lists = current_user.lists
  end

  # 詳細画面表示
  def show
    logged_in_user
    @list = List.find(params[:id])
    @items = @list.items.where.not(name: "")
    @item = @list.items.build
  end

  # 新規画面表示
  def new
    logged_in_user
    @list = List.new
  end

  # 新規作成処理
  def create
    list = current_user.lists.build(list_params)
    list.save!
    redirect_to list_path(list), notice: "リスト[#{list.title}]を登録しました。"
  end

  # 編集画面表示
  def edit
    logged_in_user
    @list = List.find(params[:id])
  end

  # 編集処理
  def update
    list = List.find(params[:id])
    list.update!(list_params)
    redirect_to list_path(list), notice: "リスト[#{list.title}]を編集しました。"
  end

  def destroy
    list = List.find(params[:id])
    list.destroy
    redirect_back(fallback_location: root_path)
  end

  private

  # リクエストパラメータ
  def list_params
    params.require(:list).permit(:title)
  end
end
