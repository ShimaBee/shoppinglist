Rails.application.routes.draw do
  get 'sessions/new'
  get  '/signup', to: 'users#new'
  post '/signup',  to: 'users#create'
  resources :users
  root 'lists#index'
  resources :lists
  resources :items, only:[:create, :destroy]
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
end
